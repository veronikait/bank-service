package com.atos.service.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages = { "com.atos.service.bank" })
@EnableJpaRepositories(basePackages = { "com.atos.service.bank.transactions.dao" })
public class ServiceBankApplication {

	public static void main(String[] args) {

		SpringApplication.run(ServiceBankApplication.class, args);

	}

}
