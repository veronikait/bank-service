package com.atos.service.bank;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class Initialization implements InitializingBean {

	private static Logger LOGGER = LoggerFactory.getLogger(Initialization.class);
	
	@Override
	public void afterPropertiesSet() throws Exception {

		InetAddress inetAddress = InetAddress.getLocalHost();

		LOGGER.info("Application listen IP: " + inetAddress.getHostAddress());

	}
}
