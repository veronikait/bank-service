package com.atos.service.bank.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * This class is useful for manage externalized messages and
 * internationalization.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
public class Messages {

	/**
	 * The Bundle name message
	 */
	private static final String BUNDLE_NAME = "messages";

	/**
	 * The resource bundle
	 */
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	/**
	 * Default constructor no args
	 */
	private Messages() {
	}

	/**
	 * Use this method for get the strings value for a key.
	 * 
	 * @param key The key in the property file.
	 * @return String value for the key.
	 */
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
