package com.atos.service.bank.transactions.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.atos.service.bank.transactions.entities.ReferenceCounterEntity;

/**
 * The Reference DAO for manage reference persistence.
 * 
 * @author Rafael Benedettelli
 * @since  1.0
 */
@Repository
@Transactional
public class ReferenceDAO extends DaoBase implements Dao<ReferenceCounterEntity> {

	@Override
	public Optional<ReferenceCounterEntity> get(long id) {

		ReferenceCounterEntity reference = entityManager.find(ReferenceCounterEntity.class, id);

		return Optional.of(reference);
	}

	@Override
	public List<ReferenceCounterEntity> getAll() {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ReferenceCounterEntity> cq = cb.createQuery(ReferenceCounterEntity.class);
		Root<ReferenceCounterEntity> rootEntry = cq.from(ReferenceCounterEntity.class);
		CriteriaQuery<ReferenceCounterEntity> all = cq.select(rootEntry);

		TypedQuery<ReferenceCounterEntity> allQuery = entityManager.createQuery(all);
		return allQuery.getResultList();

	}

	@Override
	public Long save(ReferenceCounterEntity t) {
		entityManager.persist(t);
		return t.getCounter();
	}

	@Override
	public void update(ReferenceCounterEntity t, String[] params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(ReferenceCounterEntity t) {
		// TODO Auto-generated method stub

	}

}
