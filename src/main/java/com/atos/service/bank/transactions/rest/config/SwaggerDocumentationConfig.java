
package com.atos.service.bank.transactions.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.atos.service.bank.util.Messages;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Override Swagger UI Presentation.
 * This class fill the swagger UI with
 * own business data.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
@EnableSwagger2
@Configuration
public class SwaggerDocumentationConfig {

	private static final String BASE_PACKAGE = "com.atos.service.bank"; 

	private String title=Messages.getString("configuration.title");

	private String groupName=Messages.getString("configuration.groupName");

	private String description=Messages.getString("configuration.description"); 

	private String licenseUrl=Messages.getString("configuration.url"); 

	private String termsOfServiceUrl=Messages.getString("configuration.urlTerms"); 

	private String version=Messages.getString("configuration.version"); 

	private String contact=Messages.getString("configuration.contact");

	private String contactMail=Messages.getString("configuration.mail");

	ApiInfo apiInfo() {

		return new ApiInfoBuilder().title(title).description(description).licenseUrl(licenseUrl)
				.termsOfServiceUrl(termsOfServiceUrl).version(version).contact(new Contact(contact, "", contactMail))
				.build();

	}

	@Bean
	@Primary
	public Docket customImplementation() {

		return new Docket(DocumentationType.SWAGGER_2).groupName(groupName).select()
				.apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE)).build().apiInfo(apiInfo());
	}

}