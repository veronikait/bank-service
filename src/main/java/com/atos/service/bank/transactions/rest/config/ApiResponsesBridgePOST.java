package com.atos.service.bank.transactions.rest.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Useful annotation for use in Rest POST endopoints
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
@ApiResponses(value = { 

		@ApiResponse(code = 201, message = "201 CREATED"),
		@ApiResponse(code = 400, message = "400 Bad Request"),
		@ApiResponse(code = 401, message = "401 Unauthorized"),
		@ApiResponse(code = 404, message = "404 Not Found"),
		@ApiResponse(code = 500, message = "500 Internal Server Error")

}
)

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiResponsesBridgePOST {
	
	

}