package com.atos.service.bank.transactions.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atos.service.bank.transactions.exceptions.BankException;
import com.atos.service.bank.transactions.model.SimpleResultDTO;
import com.atos.service.bank.transactions.model.StatusDTO;
import com.atos.service.bank.transactions.model.TransactionDTO;
import com.atos.service.bank.transactions.rest.config.ApiResponsesBridgeGET;
import com.atos.service.bank.transactions.rest.config.ApiResponsesBridgePOST;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * This interfaces define the controller layer for transaction model.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
@RestController
@Api(value = "transactions")
public interface TransactionServiceRest {

	final String API_OPERATION_NOTES_CREATE_POST = "This operation create new transaction in the application bank";
	
	final String API_OPERATION_NOTES_SEARCH_GET = "This operation retrieve all transaction in the application bank";
	
	final String API_OPERATION_NOTES_STATUS_POST = "This operation show the transaction status in the application bank";

	@ApiResponsesBridgePOST
	@ApiOperation(value = "", nickname = "createTransaction", notes = API_OPERATION_NOTES_CREATE_POST, response = SimpleResultDTO.class)
	@RequestMapping(value = "/banks/transactions", 
					produces = { "application/json" }, 
					consumes = { "application/json" }, 
					method = RequestMethod.POST)
	ResponseEntity<SimpleResultDTO> createTransaction(
			@ApiParam(value = "TransactionDTO", required = true) @Valid @RequestBody TransactionDTO transaction) throws BankException;
	
	@ApiResponsesBridgeGET
	@ApiOperation(value = "", nickname = "searchTransactions", notes = API_OPERATION_NOTES_SEARCH_GET, response = SimpleResultDTO.class)
	@RequestMapping(value = "/banks/transactions",
					produces = { "application/json" }, 
					method = RequestMethod.GET)
	ResponseEntity<List<TransactionDTO>> searchTransactions(
			@ApiParam(value = "iban", required = true)   @RequestParam("iban") String iban,
			@ApiParam(value = "sort", required = false)  @RequestParam(name="sort",required=false) String sort) throws BankException;
	
	@ApiResponsesBridgePOST
	@ApiOperation(value = "", nickname = "statusTransaction", notes = API_OPERATION_NOTES_STATUS_POST, response = SimpleResultDTO.class)
	@RequestMapping(value = "/banks/transactions/status", 
					produces = { "application/json" }, 
					consumes = { "application/json" }, 
					method = RequestMethod.POST)
	ResponseEntity<TransactionDTO> statusTransaction(
			@ApiParam(value = "TransactionDTO", required = true) @Valid  @RequestBody StatusDTO status) throws BankException;

}
