package com.atos.service.bank.transactions.model;

public enum Status {
	
	PENDING, SETTLED, FUTURE,INVALID

}
