package com.atos.service.bank.transactions.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.atos.service.bank.transactions.entities.TransactionEntity;

/**
 * Transaction DAO manages persistenence of transaction entities.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 */
@Repository
@Transactional
public class TransactionDAO extends DaoBase implements Dao<TransactionEntity> {

	@Override
	public Optional<TransactionEntity> get(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionEntity> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long save(TransactionEntity transactionEntity) {

		entityManager.persist(transactionEntity);
		return transactionEntity.getId();

	}

	public List<TransactionEntity> findByIban(String iban, String sortAttribute, String sortOrder) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<TransactionEntity> cq = cb.createQuery(TransactionEntity.class);
		Root<TransactionEntity> rootEntry = cq.from(TransactionEntity.class);
		CriteriaQuery<TransactionEntity> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("accountIban"), iban));

		if (sortAttribute != null) {

			Order order = null;

			if (sortOrder.toLowerCase().equals("asc")) {

				order = cb.asc(rootEntry.get(sortAttribute));

			} else {

				order = cb.desc(rootEntry.get(sortAttribute));

			}

			all.orderBy(order);

		}

		TypedQuery<TransactionEntity> allQuery = entityManager.createQuery(all);

		return allQuery.getResultList();
	}

	public List<TransactionEntity> findByReference(String reference) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();

		CriteriaQuery<TransactionEntity> cq = cb.createQuery(TransactionEntity.class);
		Root<TransactionEntity> rootEntry = cq.from(TransactionEntity.class);
		CriteriaQuery<TransactionEntity> all = cq.select(rootEntry)
				.where(cb.equal(rootEntry.get("reference"), reference));

		TypedQuery<TransactionEntity> allQuery = entityManager.createQuery(all);

		return allQuery.getResultList();
	}

	@Override
	public void update(TransactionEntity transactionDTO, String[] params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(TransactionEntity t) {
		// TODO Auto-generated method stub

	}

}
