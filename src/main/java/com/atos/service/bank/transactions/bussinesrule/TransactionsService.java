package com.atos.service.bank.transactions.bussinesrule;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atos.service.bank.transactions.dao.TransactionDAO;
import com.atos.service.bank.transactions.entities.TransactionEntity;
import com.atos.service.bank.transactions.exceptions.BadSortFormattedException;
import com.atos.service.bank.transactions.exceptions.BankException;
import com.atos.service.bank.transactions.mappers.TransactionMapper;
import com.atos.service.bank.transactions.model.Channel;
import com.atos.service.bank.transactions.model.Status;
import com.atos.service.bank.transactions.model.StatusDTO;
import com.atos.service.bank.transactions.model.TransactionDTO;

/**
 * This Class manage the business rules for transaction model.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 */
@Service
public class TransactionsService {

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionDAO transactinoDAO;

	@Autowired
	private ReferenceGeneratorService referenceGeneratorService;

	@Autowired
	private TransactionMapper transactionMapper;

	public Long createTransaction(TransactionDTO transactionDTO) throws BankException {

		if (transactionDTO.getReference() == null || transactionDTO.getReference().trim().equals("")) {

			String referenceGenerated = referenceGeneratorService.generateReference();
			transactionDTO.setReference(referenceGenerated);

		}

		TransactionEntity transactionEntity = transactionMapper.mapper(transactionDTO);
		Long transactionId = transactinoDAO.save(transactionEntity);
		accountService.updateAccountBalance(transactionDTO);

		return transactionId;

	}

	public List<TransactionDTO> searchTransactions(String iban, String sort) throws BankException {

		String sortField = null;
		String sortTye = null;

		if (sort != null) {

			if (sort.contains(":")) {

				String[] sortParts = sort.split(":");
				sortField = sortParts[0];
				sortTye = sortParts[1];

			} else {

				throw new BadSortFormattedException(
						"the sort query param must be in the format field:type (example: iban:asc)");
			}

		}

		List<TransactionEntity> transactionsEntities = transactinoDAO.findByIban(iban, sortField, sortTye);
		List<TransactionDTO> transactionsDtos = transactionMapper.mapper(transactionsEntities);

		return transactionsDtos;
	}

	public TransactionDTO searchTransactionByReference(StatusDTO statusDTO) throws BankException {

		TransactionDTO transactionDTO = null;
		List<TransactionEntity> transactionsEntities = transactinoDAO.findByReference(statusDTO.getReference());

		if (transactionsEntities != null && transactionsEntities.size() > 0) {

			TransactionEntity transactionEntity = transactionsEntities.get(0);
			transactionDTO = transactionMapper.mapper(transactionEntity);

			// Exclude properties in response by setting in null
			transactionDTO.setAccountIban(null);
			transactionDTO.setDescription(null);

			determineStatus(transactionDTO, statusDTO);
			calculateAmount(transactionDTO, statusDTO);

		} else {

			// Bussines Rule (A)
			transactionDTO = new TransactionDTO();
			transactionDTO.setReference(statusDTO.getReference());
			transactionDTO.setStatus(Status.INVALID);

		}

		// Setting date to null for exclusion in the response
		transactionDTO.setDate(null);

		return transactionDTO;
	}

	/**
	 * This method encapsulate logic for determine the transaction status.
	 * 
	 * @param transactionDTO
	 * @param statusDTO
	 */
	private void determineStatus(TransactionDTO transactionDTO, StatusDTO statusDTO) {

		LocalDate now = LocalDate.now();
		LocalDate date = transactionDTO.getDate().toLocalDate();
		Channel chanel = statusDTO.getChannel();

		Status newStatus;

		if (date.isBefore(now)) {

			newStatus = Status.SETTLED;

		} else if (date.equals(now)) {

			newStatus = Status.PENDING;

		} else {

			if (chanel.equals(Channel.ATM)) {

				newStatus = Status.PENDING;

			} else {

				newStatus = Status.FUTURE;
			}
		}

		transactionDTO.setStatus(newStatus);

	}

	/**
	 * This method encapsulate the logic for determine the total amount.
	 * 
	 * @param transactionDTO
	 * @param statusDTO
	 */
	private void calculateAmount(TransactionDTO transactionDTO, StatusDTO statusDTO) {

		Channel channel = statusDTO.getChannel();

		switch (channel) {

		case ATM:
		case CLIENT:

			BigDecimal amount = transactionDTO.getAmount();
			BigDecimal subtracted = amount.subtract(transactionDTO.getFee());
			transactionDTO.setAmount(subtracted);
			transactionDTO.setFee(null);

			break;

		default:
			break;
		}
	}

}
