package com.atos.service.bank.transactions.dao;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * DAO Base for helper subtypes.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
public class DaoBase {

	@Autowired
	protected EntityManager entityManager;


}
