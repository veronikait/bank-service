package com.atos.service.bank.transactions.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AccountDTO {
	
	private String accountIban;
	
	private BigDecimal balance;


}
