package com.atos.service.bank.transactions.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonInclude;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(value = "reference,accountIban,date,fee,description")
public class TransactionDTO {

	private String reference;

	@NotNull
	@Size(min = 24, max = 24)
	private String accountIban;

	@NotNull
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private LocalDateTime date;

	@NotNull
	@DecimalMin(value = "-1000000000", inclusive = true)
	@DecimalMax(value = "1000000000", inclusive = true)
	private BigDecimal amount;

	@NotNull
	@DecimalMin(value = "0.1", inclusive = true)
	@DecimalMax(value = "1000000000", inclusive = true)
	private BigDecimal fee;

	@NotNull
	@Size(min = 5, max = 50)
	private String description;

	private Status status;

	

	public BigDecimal getAmount() {

		if (amount != null) {

			amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		}

		return amount;
	}


	public BigDecimal getFee() {

		if (fee != null) {

			fee = fee.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		}

		return fee;
	}


}
