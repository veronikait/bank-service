package com.atos.service.bank.transactions.exceptions;

import com.atos.service.bank.util.Messages;

/**
 * Custom exception for throw when bad formatter sort.
 * 
 * @author Rafael Benedettelli
 *
 */
public class BadSortFormattedException extends BankException {

	/**
	 * Default Serial Key
	 */
	private static final long serialVersionUID = 1L;

	public BadSortFormattedException(String isban) {

		super(Messages.getString("exception.bad_sort_format.message")  + isban);

	}

}
