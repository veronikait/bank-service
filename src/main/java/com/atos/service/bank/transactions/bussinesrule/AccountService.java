package com.atos.service.bank.transactions.bussinesrule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.atos.service.bank.transactions.dao.AccountDAO;
import com.atos.service.bank.transactions.entities.AccountEntity;
import com.atos.service.bank.transactions.exceptions.AccountNotFoundException;
import com.atos.service.bank.transactions.model.TransactionDTO;

/**
 * This Service Class belongs to Business Rule and manage the business logic of
 * transaction model.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
@Service
public class AccountService {

	private static Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

	@Autowired
	private AccountDAO accountDAO;

	/**
	 * This method update the acount balance.
	 * 
	 * @param transaction the transaction DTO.
	 * @throws AccountNotFoundException if not account has found.
	 */
	public void updateAccountBalance(TransactionDTO transaction) throws AccountNotFoundException {

		AccountEntity accountDTO = accountDAO.findByIban(transaction.getAccountIban())
				.orElseThrow(() -> new AccountNotFoundException(transaction.getAccountIban()));

		BigDecimal newBalance = accountDTO.getBalance().add(transaction.getAmount());

		accountDTO.setBalance(newBalance);

		LOGGER.debug("El balance es " + newBalance);

		accountDAO.save(accountDTO);

	}

	@Bean
	public AccountEntity generateFirstSystemAccount() {

		final String DEFAULT_ACCOUNT_NUMBER = "ES9820385778983000760236";

		LOGGER.debug("Generating the first default account " + DEFAULT_ACCOUNT_NUMBER);

		// Initialize system with one example accounts
		AccountEntity accountEntity = new AccountEntity();
		accountEntity.setAccountIban(DEFAULT_ACCOUNT_NUMBER);
		accountEntity.setBalance(new BigDecimal(0));

		accountDAO.save(accountEntity);

		return accountEntity;
	}

}
