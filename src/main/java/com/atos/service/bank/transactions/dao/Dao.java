package com.atos.service.bank.transactions.dao;

import java.util.List;
import java.util.Optional;

/**
 * DAO Generic interface 
 * define the main methods for CRUD Entities 
 * objects.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 * @param <T>
 */
public interface Dao<T> {

	Optional<T> get(long id);

	List<T> getAll();

	Long save(T t);

	void update(T t, String[] params);

	void delete(T t);
}
