package com.atos.service.bank.transactions.rest.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.atos.service.bank.transactions.bussinesrule.TransactionsService;
import com.atos.service.bank.transactions.exceptions.BankException;
import com.atos.service.bank.transactions.model.SimpleResultDTO;
import com.atos.service.bank.transactions.model.StatusDTO;
import com.atos.service.bank.transactions.model.TransactionDTO;
import com.atos.service.bank.transactions.rest.TransactionServiceRest;

/**
 * This class is the controller layer for transaction model
 * Manages all request and responses over Rest protocol for clients.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
@Controller
public class TransactionServiceRestImpl implements TransactionServiceRest {

	/**
	 * Transaction services
	 */
	@Autowired
	private TransactionsService transactionService;

	@Override
	public ResponseEntity<SimpleResultDTO> createTransaction(@Valid TransactionDTO transactionDTO)
			throws BankException {

		Long id = transactionService.createTransaction(transactionDTO);

		SimpleResultDTO simpleResultDTO = new SimpleResultDTO();

		simpleResultDTO.setId(id);

		return new ResponseEntity<>(simpleResultDTO, HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<List<TransactionDTO>> searchTransactions(String iban, String sort) throws BankException {

		List<TransactionDTO> transactions = transactionService.searchTransactions(iban, sort);

		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<TransactionDTO> statusTransaction(StatusDTO statusDTO) throws BankException {

		TransactionDTO transaction = transactionService.searchTransactionByReference(statusDTO);

		return new ResponseEntity<>(transaction, HttpStatus.OK);

	}

}
