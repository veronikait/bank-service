package com.atos.service.bank.transactions.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.atos.service.bank.transactions.entities.AccountEntity;

/**
 * DAO for manage account persistence.
 * 
 * @author Rafael Benedettelli
 * @since 1.0
 *
 */
@Repository
@Transactional
public class AccountDAO extends DaoBase implements Dao<AccountEntity> {

	@Override
	public Optional<AccountEntity> get(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Optional<AccountEntity> findByIban(String iban) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AccountEntity> cq = cb.createQuery(AccountEntity.class);
		Root<AccountEntity> rootEntry = cq.from(AccountEntity.class);
		CriteriaQuery<AccountEntity> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("accountIban"), iban));

		TypedQuery<AccountEntity> allQuery = entityManager.createQuery(all);
		return allQuery.getResultStream().findAny();

	}

	@Override
	public List<AccountEntity> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long save(AccountEntity accountEntity) {
		entityManager.persist(accountEntity);
		return accountEntity.getId();
	}

	@Override
	public void update(AccountEntity t, String[] params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(AccountEntity t) {
		// TODO Auto-generated method stub

	}

}
