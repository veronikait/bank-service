package com.atos.service.bank.transactions.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.atos.service.bank.transactions.entities.TransactionEntity;
import com.atos.service.bank.transactions.model.TransactionDTO;

@Mapper
public interface TransactionMapper {

	TransactionDTO mapper(TransactionEntity transactionEntity);

	TransactionEntity mapper(TransactionDTO transactionDTO);

	List<TransactionDTO> mapper(List<TransactionEntity> transactionEntity);

}
