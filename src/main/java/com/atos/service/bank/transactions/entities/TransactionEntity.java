package com.atos.service.bank.transactions.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name="transactions")
public class TransactionEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String reference;
	
	private String accountIban;
	
	private LocalDateTime date;
	
	private BigDecimal amount;
	
	private BigDecimal fee;
	
	private String description;


}
