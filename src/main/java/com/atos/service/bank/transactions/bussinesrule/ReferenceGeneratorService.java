package com.atos.service.bank.transactions.bussinesrule;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atos.service.bank.transactions.dao.ReferenceDAO;
import com.atos.service.bank.transactions.entities.ReferenceCounterEntity;

@Service
public class ReferenceGeneratorService {

	private static Logger LOGGER = LoggerFactory.getLogger(ReferenceGeneratorService.class);

	/**
	 * DAO reference.
	 */
	@Autowired
	private ReferenceDAO referenceDAO;

	/**
	 * Dafault no args constructor
	 */
	public ReferenceGeneratorService() {
		
	}

	/**
	 * Use for generate new reference code
	 * @return
	 */
	public String generateReference() {

		LOGGER.debug("Generate new reference");

		final int FIRST_ELEMENT = 0;
		final int INCREMENTER = 1;
		String reference = "";
		ReferenceCounterEntity referenceCounterEntity = null;

		List<ReferenceCounterEntity> referenceCounters = referenceDAO.getAll();

		if (referenceCounters == null || referenceCounters.size() == 0) {

			LOGGER.debug("Begins reference counter");

			reference = "0";
			referenceCounterEntity = new ReferenceCounterEntity(new Long(reference));

		} else {

			LOGGER.debug("Increments reference counter");

			referenceCounterEntity = referenceCounters.get(FIRST_ELEMENT);
			referenceCounterEntity.setCounter(referenceCounterEntity.getCounter() + INCREMENTER);
			reference = String.valueOf(referenceCounterEntity.getCounter());

		}

		referenceDAO.save(referenceCounterEntity);
		String newReference = String.format("%1$5s", reference).replace(' ', '0');

		return newReference;

	}

}
