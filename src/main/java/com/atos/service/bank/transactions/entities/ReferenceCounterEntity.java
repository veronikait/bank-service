package com.atos.service.bank.transactions.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity(name = "reference_counter")
public class ReferenceCounterEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "counter")
	private Long counter;

	public ReferenceCounterEntity(Long counter) {

		this.counter = counter;
	}

	public ReferenceCounterEntity() {

	}


	

}
