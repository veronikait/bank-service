package com.atos.service.bank.transactions.model;

public enum Channel {
	CLIENT, ATM, INTERNAL
}
