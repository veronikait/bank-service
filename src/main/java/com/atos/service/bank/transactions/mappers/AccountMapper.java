package com.atos.service.bank.transactions.mappers;

import org.mapstruct.Mapper;

import com.atos.service.bank.transactions.entities.AccountEntity;
import com.atos.service.bank.transactions.model.AccountDTO;

/**
 * This Mapper maps objects attributes between Entities and DTO.
 * 
 * @author Rafael Benedettelli
 *
 */
@Mapper
public interface AccountMapper {

	AccountDTO mapper(AccountEntity accountEntity);

	AccountEntity mapper(AccountDTO accountDTO);

}
