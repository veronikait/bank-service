package com.atos.service.bank.transactions.exceptions;

/**
 * Custom exception base model for all bank custom exceptions.
 * 
 * @author Rafael Benedettelli
 *
 */
public class BankException extends Exception {

	/**
	 * Default Serial Key
	 */
	private static final long serialVersionUID = 1L;

	public BankException(String message) {

		super(message);

	}
}
