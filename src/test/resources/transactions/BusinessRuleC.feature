Feature: Transaction past INTERNAL stored is SETTLED
    Check the status from INTERNAL channel  
    
  Scenario: 
	Given A transaction that is stored in our system
	When I check the status from INTERNAL channel And the transaction date is before today
	Then The system returns the status "SETTLED" And the amount and the fee
	