Feature: Transaction FUTURE INTERNAL stored is FUTURE
    Check the status from INTERNAL channel  
    
  Scenario: 
	Given A transaction that is stored in our system
	When  I check the status from INTERNAL channel And the transaction date is greater than today
	Then The system returns the status "FUTURE" And the amount and the fee
