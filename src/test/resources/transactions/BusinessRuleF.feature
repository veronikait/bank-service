Feature: Transaction FUTURE CLIENT stored is FUTURE
    Check the status from CLIENT channel  
    
  Scenario: 
	Given A transaction that is stored in our system
	When  I check the status from CLIENT channel And the transaction date is greater than today
	Then The system returns the status "FUTURE" And the amount substracting the fee
	