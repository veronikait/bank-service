Feature: Transaction PRESENT INTERNAL stored is PENDING
    Check the status from INTERNAL channel  
    
  Scenario: 
	Given A transaction that is stored in our system
	When  I check the status from INTERNAL channel And the transaction date is equals to today
	Then The system returns the status "PENDING" And the amount and the fee D