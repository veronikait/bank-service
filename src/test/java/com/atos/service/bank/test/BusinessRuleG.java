package com.atos.service.bank.test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.atos.service.bank.transactions.model.Channel;
import com.atos.service.bank.transactions.model.SimpleResultDTO;
import com.atos.service.bank.transactions.model.StatusDTO;
import com.atos.service.bank.transactions.model.TransactionDTO;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BusinessRuleG {

	private static Logger LOGGER = LoggerFactory.getLogger(BusinessRuleG.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TransactionDTO transactionDTO;

	private TransactionDTO transactionStatus;

	private StatusDTO statusDTO = new StatusDTO();

	@Given("Transaction FUTURE ATM stored is FUTURE")
	public void Transaction_FUTURE_ATM_stored_is_FUTURE() {

		transactionDTO.setDate(LocalDateTime.now().plusMonths(1));

		restTemplate.postForObject("/banks/transactions", transactionDTO, SimpleResultDTO.class);

		statusDTO.setReference("00000");

	}

	@When("I check the status from ATM channel And the transaction date is greater than today")
	public void I_check_the_status_from_ATM_channel_And_the_transaction_date_is_greater_than_today() {

		statusDTO.setChannel(Channel.ATM);

		transactionStatus = restTemplate.postForObject("/banks/transactions/status", statusDTO, TransactionDTO.class);
	}

	@Then("The system returns the status {string} And the amount substractiong the fee G")
	public void The_system_returns_the_status_And_the_amount_and_the_fee_E(String expectedAnswer) {

		assertEquals(expectedAnswer, transactionStatus.getStatus().toString());

		BigDecimal amountSubtractFee = transactionDTO.getAmount().subtract(transactionDTO.getFee());

		assertEquals(transactionStatus.getAmount(), amountSubtractFee);

		LOGGER.info("Amount: " + transactionDTO.getAmount());
		LOGGER.info("Fee: " + transactionDTO.getFee());
	}
}