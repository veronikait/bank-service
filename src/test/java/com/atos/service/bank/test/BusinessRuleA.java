package com.atos.service.bank.test;

import static org.junit.Assert.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.atos.service.bank.transactions.model.Channel;
import com.atos.service.bank.transactions.model.StatusDTO;
import com.atos.service.bank.transactions.model.TransactionDTO;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BusinessRuleA {

	@Autowired
	private RestTemplate restTemplate;

	private TransactionDTO transactionStatus;

	private StatusDTO statusDTO = new StatusDTO();

	@Given("Transaction of any chanel not stored is invalid")
	public void Transaction_of_any_chanel_not_stored_is_invalid() {

		statusDTO.setReference("00000");

	}

	@When("I check the status from any channel")
	public void I_check_the_status_from_any_channel() {

		statusDTO.setChannel(Channel.ATM);

		transactionStatus = restTemplate.postForObject("/banks/transactions/status", statusDTO, TransactionDTO.class);
	}

	@Then("The system returns the status {string}")
	public void The_system_returns_the_status(String expectedAnswer) {
		assertEquals(expectedAnswer, transactionStatus.getStatus().toString());
	}
}