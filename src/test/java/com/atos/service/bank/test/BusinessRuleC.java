package com.atos.service.bank.test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.atos.service.bank.transactions.model.Channel;
import com.atos.service.bank.transactions.model.SimpleResultDTO;
import com.atos.service.bank.transactions.model.StatusDTO;
import com.atos.service.bank.transactions.model.TransactionDTO;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class BusinessRuleC {

	private static Logger LOGGER = LoggerFactory.getLogger(BusinessRuleC.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TransactionDTO transactionDTO;

	private TransactionDTO transactionStatus;

	private StatusDTO statusDTO = new StatusDTO();

	@Given("Transaction past INTERNAL stored is SETTLED")
	public void Transaction_past_INTERNAL_stored_is_SETTLED() {

		transactionDTO.setDate(LocalDateTime.now().minusMonths(1));

		restTemplate.postForObject("/banks/transactions", transactionDTO, SimpleResultDTO.class);

		statusDTO.setReference("00000");

	}

	@When("I check the status from INTERNAL channel And the transaction date is before today")
	public void I_check_the_status_from_CLIENT_or_ATM_channel_And_the_transaction_date_is_before_today() {

		statusDTO.setChannel(Channel.INTERNAL);

		transactionStatus = restTemplate.postForObject("/banks/transactions/status", statusDTO, TransactionDTO.class);
	}

	@Then("The system returns the status {string} And the amount and the fee")
	public void The_system_returns_the_status_And_the_amount_and_the_fee(String expectedAnswer) {

		assertEquals(expectedAnswer, transactionStatus.getStatus().toString());

		assertEquals(transactionStatus.getAmount(), transactionStatus.getAmount());

		LOGGER.info("Amount: " + transactionDTO.getAmount());
	}
}