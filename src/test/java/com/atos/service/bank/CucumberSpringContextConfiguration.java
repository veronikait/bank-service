package com.atos.service.bank;

import org.springframework.boot.test.context.SpringBootContextLoader;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;

import com.atos.service.bank.ServiceBankApplication;

import io.cucumber.java.Before;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = ServiceBankApplication.class, loader = SpringBootContextLoader.class)
public class CucumberSpringContextConfiguration {

	@Before
	public void setUp() {
		System.out.println("-------------- Spring Context Initialized For Executing Cucumber Tests --------------");
	}
}
