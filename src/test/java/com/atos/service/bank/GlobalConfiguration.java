package com.atos.service.bank;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import com.atos.service.bank.transactions.model.TransactionDTO;

@Configuration
public class GlobalConfiguration {

	@Bean
	public RestTemplate buildRest() {

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory("http://127.0.0.1:8080"));

		return restTemplate;
	}
	
	@Bean
	public TransactionDTO testTransaction() {
		
		TransactionDTO transactionDTO = new TransactionDTO();
		transactionDTO.setAccountIban("ES9820385778983000760236");
		transactionDTO.setAmount(new BigDecimal(100));
		transactionDTO.setDate(LocalDateTime.now());
		transactionDTO.setDescription("Bar coffe");
		transactionDTO.setFee(new BigDecimal(1));
		return transactionDTO;
	}

}
