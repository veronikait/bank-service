package com.atos.service.bank;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", features = "src/test/resources/transactions")
@SpringBootTest
public class ServiceBankApplicationTests {

	@Test
	public void contextLoads() {
	}

}
