## Service Bank

![](https://www.incimages.com/uploaded_files/image/970x450/getty_158673029_9707279704500119_78594.jpg)

### Features

- Microservice java backend for process transactions bank.
- Create bank transactions
- Search bank transactions
- Check status for bank transactions

### Deploy from code
- execute git clone https://gitlab.com/veronikait/bank-service
- execute mvn clean install (All test will run)
- execute mvn spring-boot:run 
- In the base directory project there is a postman collection for use the microservice
- Also you can see http://127.0.0.1:8080/swagger-ui.html for swagger UI microservice

### Test
 - ATDD integration testing
 - I used cucumber integrated with junit and spring for test business rules.
 - For test all execute mvn test


### Release notes

-   I decided to use Spring boot framework as structural base framework for microservice solution.
-	The solution was implemented in differents layers. (RestController, BuisinessRule and DAO)
-  	Controller (for Rest Api requests)
- 	Bussines Rule (For resolve the bussines rules)
- 	DAO (JPA for persistance)
-   The project uses DTO (Data Tranfer Object for business and controller layer) and Entity for DAO layer.

### Other frameworks and platforms

- I decided to use H2 Hypersonic in memory database.
- For transformation from Entities for DTO POJO's, I use MapStruct.
- For automatically getter and setter generator, I use Lombok.
- For project manager I opted for Maven.
- The Microservice presentation is in Swagger UI.
- The swagger UI messages and exception messages are externalized.





